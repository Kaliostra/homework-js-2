"use strict";

// 1. Цикл нужен для работы с числами, с их помошью можно задавать числу определенный шаг, также можно вытянуть определенное значение из цикла.
// 2. В данной задаче я использовал цикл For для того чтобы задать числу кратность 5 и чтобы мне показало все числа кратно 5 до числа которое задал рользователь.
// 3.Явное-это String, Number,Boolean.
//  Не явное- это не логическое преобразование пример 12 / "6", когда число делять на текст 6.

let s;
do {
  s = prompt("Вкажіть число");
} while (s === null || s === "" || isNaN(s));
if (s < 5) {
  alert("Sorry, no numbers");
}
for (let i = 0; i <= s; i = i + 5) {
  console.log(i);
}
